<?php
    namespace app\models;
    use app\models\Question;
    use app\models\Answer;

    class QuestionValidator{

            public function getQuestionInputError(array $data): string{
                $error = "Nebylo zadáno: ";
                if(empty($data['questionText'])) $error .= "Text otázky, ";
                if(empty($data['answerType'])) $error .= "Typ odpovědi, ";
                if(empty($data['test'])) $error .= "ID testu, ";
                return $error;
            }
    }