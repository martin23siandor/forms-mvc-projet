<?php
    namespace app\models;

    class ExamAnswer{
        private int $id;
        private ?string $textAnswer = null;
        private ?string $abcAnswer = null;
        private int $question;
        private int $exam;

        /**
         * @return int
         */
        public function getId(): int
        {
            return $this->id;
        }

        /**
         * @param int $id
         */
        public function setId(int $id): void
        {
            $this->id = $id;
        }

        /**
         * @return string|null
         */
        public function getTextAnswer(): ?string
        {
            return $this->textAnswer;
        }

        /**
         * @param string|null $textAnswer
         */
        public function setTextAnswer(?string $textAnswer): void
        {
            $this->textAnswer = $textAnswer;
        }

        /**
         * @return string|null
         */
        public function getAbcAnswer(): ?string
        {
            return $this->abcAnswer;
        }

        /**
         * @param string|null $abcAnswer
         */
        public function setAbcAnswer(?string $abcAnswer): void
        {
            $this->abcAnswer = $abcAnswer;
        }

        /**
         * @return int
         */
        public function getQuestion(): int
        {
            return $this->question;
        }

        /**
         * @param int $question
         */
        public function setQuestion(int $question): void
        {
            $this->question = $question;
        }

        /**
         * @return int
         */
        public function getExam(): int
        {
            return $this->exam;
        }

        /**
         * @param int $exam
         */
        public function setExam(int $exam): void
        {
            $this->exam = $exam;
        }


    }