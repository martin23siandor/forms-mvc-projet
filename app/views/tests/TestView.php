<?php use app\models\Test; ?>
<div class="addButton"><a href=<?php echo "index.php?controller=Tests&action=add"?>>Přidat test</a></div>
<?php
    foreach($model as $test) {
        if ($test instanceof Test) {


            echo "<div class='item'>
            <h1>{$test->getName()} </h1>
            <h3>min. úspěšnost: {$test->getMinSuccess()}%</h3>
            <div class='itemItems'>
                <div class='itemA'>
                <a href='index.php?controller=Questions&test={$test->getId()}&testName={$test->getName()}'>Editovat test</a>
                </div>
                <div class='itemA'>
                    <a href='index.php?controller=Tests&action=fill&test={$test->getId()}&testName={$test->getName()}'>Vypracovat test</a>
                </div>
            </div>
            <div>
                <div class='addButton'>
                    <a href='index.php?controller=Exam&test={$test->getId()}&testName={$test->getName()}'>Vypracované testy</a>
                </div>
                <div class='deleteButton' style='margin-top: -20px'>
                <a href='index.php?controller=Tests&action=delete&test={$test->getId()}'>Odstranit test</a>
                </div>  
            </div>
             
        </div>";
        }
    }
?>