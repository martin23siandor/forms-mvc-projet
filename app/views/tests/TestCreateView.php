<form class="addForm" action="index.php?controller=Tests&action=create" method="post">
    <?php
        echo "<p class='error'>{$message}</p>" ?? "";
    ?>
    <label>
        Název testu:
        <input type="text" class="input" name="name">
    </label>
    <label>
        Minimální úspěšnost:
        <input type="number" class="input" name="minSuccess">
    </label>
    <input type="submit" class="submitButton">
</form>