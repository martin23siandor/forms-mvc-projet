<?php
    namespace app\models;

    class User{
        private int $id;
        private string $username;
        private string $name;
        private string $surname;
        private ?int $age = null;
        private string $sex;

        /**
         * @return int
         */
        public function getId(): int
        {
            return $this->id;
        }

        /**
         * @param int $id
         */
        public function setId(int $id): void
        {
            $this->id = $id;
        }

        /**
         * @return string
         */
        public function getUsername(): string
        {
            return $this->username;
        }

        /**
         * @param string $username
         */
        public function setUsername(string $username): void
        {
            $this->username = $username;
        }

        /**
         * @return string
         */
        public function getName(): string
        {
            return $this->name;
        }

        /**
         * @param string $name
         */
        public function setName(string $name): void
        {
            $this->name = $name;
        }

        /**
         * @return string
         */
        public function getSurname(): string
        {
            return $this->surname;
        }

        /**
         * @param string $surname
         */
        public function setSurname(string $surname): void
        {
            $this->surname = $surname;
        }

        /**
         * @return int|null
         */
        public function getAge(): ?int
        {
            return $this->age;
        }

        /**
         * @param int|null $age
         */
        public function setAge(?int $age): void
        {
            $this->age = $age;
        }

        /**
         * @return string
         */
        public function getSex(): string
        {
            return $this->sex;
        }

        /**
         * @param string $sex
         */
        public function setSex(string $sex): void
        {
            $this->sex = $sex;
        }


    }