<?php
    namespace app\models;

    class Question{

        private int $id;
        private string $questionText;
        private ?string $answerType = null;
        private int $test;

        /**
         * @return int
         */
        public function getTest(): int
        {
            return $this->test;
        }

        /**
         * @param int $test
         */
        public function setTest(int $test): void
        {
            $this->test = $test;
        }

        /**
         * @return int
         */
        public function getId(): int
        {
            return $this->id;
        }

        /**
         * @param int $id
         */
        public function setId(int $id): void
        {
            $this->id = $id;
        }

        /**
         * @return string
         */
        public function getQuestionText(): string
        {
            return $this->questionText;
        }

        /**
         * @param string $questionText
         */
        public function setQuestionText(string $questionText): void
        {
            $this->questionText = $questionText;
        }

        /**
         * @return string|null
         */
        public function getAnswerType(): ?string
        {
            return $this->answerType;
        }

        /**
         * @param string|null $answerType
         */
        public function setAnswerType(?string $answerType): void
        {
            $this->answerType = $answerType;
        }
    }