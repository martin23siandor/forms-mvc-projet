<?php
    namespace app\controllers;

    abstract class BaseController{
        public function view(string $viewName, mixed $model, ?string $message = null): void{
            include __DIR__ ."/../views/{$viewName}View.php";
        }

        public function redirect(string $controller): void{
            /*header("Location: index.php?".$controller);
            exit();*/
            $path = 'index.php?controller='.$controller;
            echo "<script>window.location.href='{$path}'</script>";
        }
    }