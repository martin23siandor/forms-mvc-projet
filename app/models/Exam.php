<?php
    namespace app\models;

    class Exam{
        private int $id;
        private ?int $result = null;
        private int $user;
        private int $test;
        private string $createdOn;

        /**
         * @return int
         */
        public function getId(): int
        {
            return $this->id;
        }

        /**
         * @param int $id
         */
        public function setId(int $id): void
        {
            $this->id = $id;
        }

        /**
         * @return int|null
         */
        public function getResult(): ?int
        {
            return $this->result;
        }

        /**
         * @param int|null $result
         */
        public function setResult(?int $result): void
        {
            $this->result = $result;
        }

        /**
         * @return int
         */
        public function getUser(): int
        {
            return $this->user;
        }

        /**
         * @param int $user
         */
        public function setUser(int $user): void
        {
            $this->user = $user;
        }

        /**
         * @return int
         */
        public function getTest(): int
        {
            return $this->test;
        }

        /**
         * @param int $test
         */
        public function setTest(int $test): void
        {
            $this->test = $test;
        }

        /**
         * @return string
         */
        public function getCreatedOn(): string
        {
            return $this->createdOn;
        }

        /**
         * @param string $createdOn
         */
        public function setCreatedOn(string $createdOn): void
        {
            $this->createdOn = $createdOn;
        }


    }