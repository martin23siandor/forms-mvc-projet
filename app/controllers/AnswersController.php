<?php
    namespace app\controllers;
    use app\controllers\mappers\Mapper;
    use app\models\Answer;
    use app\models\AnswerValidator;
    use app\models\QuestionValidator;
    use app\models\DatabaseConnection;

    class AnswersController extends BaseController {

        public function edit(): void{
            $db = new DatabaseConnection();
            $data = $db->getDataFromDB("answers", "*", "where question=".$_GET['question'])->fetch_all(MYSQLI_ASSOC);

            $answerMpr = new AnswerValidator();
            $mapper = new Mapper();
            $error = '';

            $answers = [];
            if(!empty($data)){
                foreach($data as $answer){
                    $answerInstance = new Answer();
                    $mapper->fillInstance($answer, $answerInstance);
                    $answers[] = $answerInstance;
                }
            }
            else{
                $error .= "Nebyly nalezeny žádné odpovědi.";
            }

            $this->view("answers/AnswersDetail", $answers, $error);
        }

        public function update(): void{
            $db = new DatabaseConnection();

            $answerValidator = new AnswerValidator();
            $mapper = new Mapper();

            if(empty($_POST['id'])) $_POST['id'] = $_GET['answer'] ?? 0;

            $answer = new Answer();

            if(empty($_POST['question'])) $_POST['question'] = 0;
            if(empty($_POST['correctness'])) $_POST['correctness'] = null;

            $mapper->fillInstance($_POST, $answer);
            $answers[] = $answer;

            if(!empty($_POST['answerText'])){
                if($_POST['correctness']!=0){
                    $db->updateDataFromDB(
                        "answers",
                        "SET answerText='{$answer->getAnswerText()}', correctness=1",
                        "where id = {$_GET['answer']}"
                    );
                }
                else{
                    $db->updateDataFromDB(
                        "answers",
                        "SET answerText='{$answer->getAnswerText()}', correctness=0",
                        "where id = {$_GET['answer']}"
                    );
                }

                $this->redirect('Answers&action=edit&question='.$_GET['question']);
            }
            else $this->view("answers/AnswersDetail", $answers, $answerValidator->getAnswerInputError($_POST));
        }

        public function create(): void{
            $db = new DatabaseConnection();

            $answerValidator = new AnswerValidator();
            $mapper = new Mapper();

            if(empty($_POST['id'])) $_POST['id'] = 0;

            $answer = new Answer();
            $mapper->fillInstance($_POST, $answer);
            $answers[] = $answer;

            if(!empty($_POST['answerText'] && $_POST['question'])){
                if($answer->isCorrectness()!=0){
                    $db->insertDataToDB(
                        "answers",
                        " answerText, question, correctness",
                        "'{$answer->getAnswerText()}', {$answer->getQuestion()}, {$answer->isCorrectness()}"
                    );
                }
                else{
                    $db->insertDataToDB(
                        "answers",
                        " answerText, question, correctness",
                        "'{$answer->getAnswerText()}', {$answer->getQuestion()}, 0"
                    );
                }

                $this->redirect('Answers&action=edit&question='.$answer->getQuestion());
            }
            else{
                $this->view("answers/AnswersDetail",$answers,$answerValidator->getAnswerInputError($_POST));
            }
        }

        public function delete(): void{
            $db = new DatabaseConnection();
            $db->deleteDataFromDB("answers","id={$_GET['answer']}");
            $this->redirect('Answers&action=edit&question='.$_GET['question']);
        }
    }