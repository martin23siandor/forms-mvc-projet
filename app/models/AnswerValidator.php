<?php
    namespace app\models;
    use app\models\Answer;

    class AnswerValidator{

        public function getAnswerInputError(array $data): string{
            $error = "Nebylo zadáno: ";
            if(empty($data['answerText'])) $error .= "Text odpovědi, ";
            if(empty($data['question'])) $error .= "ID otázky, ";
            if(empty($data['correctness'])) $error .= "správnost odpovědi, ";
            return $error;
        }
    }