<?php
    namespace app\controllers;

    use app\controllers\mappers\Mapper;
    use app\models\DatabaseConnection;
    use app\models\User;
    use app\models\UserValidator;

    class UsersController extends BaseController{
        public function index(): void{
            $db = new DatabaseConnection();
            $data = $db->getDataFromDB("users", "*", "")->fetch_all(MYSQLI_ASSOC);

            $userValidator = new UserValidator();
            $mapper = new Mapper();
            $userInstance = new User();

            $users = [];

            $mapper->fillMultipleInstances($data, $userInstance, $users);

            $this->view("users/Users", $users);
        }

        public function edit(): void{
            $db = new DatabaseConnection();
            $data = $db->getDataFromDB("users", "*", "where id = ".$_GET['user'])->fetch_assoc();

            $userValidator = new UserValidator();
            $mapper = new Mapper();
            $user = new User();

            if(!empty($data)){
                $mapper->fillInstance($data, $user);
            }
            $this->view("Users/UsersDetail",$user);
        }

        public function add(): void{
            $data = "Ahoj";
            $this->view('users/UsersCreate',$data);
        }

        public function create(): void{
            $db = new DatabaseConnection();

            $userValidator = new UserValidator();
            $mapper = new Mapper();

            if(empty($_POST['id'])) $_POST['id'] = 0;
            if(empty($_POST['age'])) $_POST['age'] = 0;

            $user = new User();
            $mapper->fillInstance($_POST, $user);

            if(!empty($_POST['username'])&&!empty($_POST['name'])&&!empty($_POST['surname'])&&!empty($_POST['age'])&&!empty($_POST['sex'])&&$_POST['age']!=0){
                $db->insertDataToDB(
                    "users",
                    " username, name, surname, age, sex",
                    "'{$user->getUsername()}','{$user->getName()}','{$user->getSurname()}',{$user->getAge()},'{$user->getSex()}'"
                );
                $this->redirect('Users');
            }
            else  $this->view("users/UsersCreate",$user, $userValidator->getUserInputError($_POST));
        }

        public function update(): void{
            $db = new DatabaseConnection();

            $userValidator = new UserValidator();
            $mapper = new Mapper();

            $user = new User();

            if(empty($_POST['age'])) $_POST['age'] = 0;

            $mapper->fillInstance($_POST, $user);

            if(!empty($_POST['username'])&!empty($_POST['name'])&!empty($_POST['surname'])&!empty($_POST['age'])&!empty($_POST['sex'])){
                $db->updateDataFromDB(
                    "users",
                    "SET username='{$user->getUsername()}', name='{$user->getName()}', surname='{$user->getSurname()}', age={$user->getAge()}, sex='{$user->getSex()}'",
                    "where id = ".$user->getId()
                );
                $this->redirect('Users');
            }
            else{
                $this->view("users/UsersDetail",$user, $userValidator->getUserInputError($_POST));
            }


        }

        public function delete(): void{
            $db = new DatabaseConnection();
            $db->deleteDataFromDB("users","id={$_GET['user']}");
            $this->redirect('Users');
        }
    }