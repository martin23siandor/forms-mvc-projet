<?php
    namespace app\controllers;

    use app\models\QuestionValidator;
    use app\models\Answer;
    use app\models\DatabaseConnection;
    use app\models\Question;
    use app\controllers\mappers\Mapper;

    class QuestionsController extends BaseController {

        public function index(): void{
            $db = new DatabaseConnection();
            $data = $db->getDataFromDB("questions", "*",empty($_GET['test'])?"":'where test = '.$_GET['test'])->fetch_all(MYSQLI_ASSOC);

            $mapper = new Mapper();
            $error = "";

            $questions = [];
            $questionInstance = new Question();

            !empty($data) ? $mapper->fillMultipleInstances($data, $questionInstance, $questions) : $error .= "Nejsou dostupná žádná data.";

            $this->view("questions/Questions", $questions, $error);
        }

        public function edit(): void{
            $db = new DatabaseConnection();
            $data = $db->getDataFromDB("questions", "*", "where id = ".$_GET['question'])->fetch_assoc();

            $mapper = new Mapper();
            $questionInstance = new Question();

            $error = '';
            if(!empty($data)) $mapper->fillInstance($data, $questionInstance);
            else{
                $question= "";
                $error .= "Nejsou dostupná žádná data.";
            }

            $this->view("questions/QuestionsDetail", $questionInstance, $error);
        }

        public function update(): void{
            $db = new DatabaseConnection();

            $mapper = new Mapper();
            $question = new Question();
            $questionValidator = new QuestionValidator();

            if(empty($_POST['id'])) $_POST['id'] = $_GET['question'];
            if(empty($_POST['answerType'])) $_POST['answerType'] = null;
            if(empty($_POST['test'])) $_POST['test'] = 0;
            $mapper->fillInstance($_POST, $question);

            if(!empty($_POST['questionText'])){

                $db->updateDataFromDB(
                    "questions",
                    "SET questionText='{$question->getQuestionText()}'",
                    "where id = ".$_GET['question']
                );
                $this->redirect('Questions&action=edit&question='.$_GET['question']);
            }
            else{
                $this->view("questions/QuestionsDetail",$question,$questionValidator->getQuestionInputError($_POST));
            }

        }

        public function add(): void{
            $data = '';
            $this->view('questions/QuestionsCreate', $data);
        }

        public function create(): void{
            $db = new DatabaseConnection();

            $mapper = new Mapper();
            $question = new Question();
            $questionValidator = new QuestionValidator();
            if(empty($_POST['id'])) $_POST['id'] = $_GET['question'] ?? 0;
            if(empty($_POST['test'])) $_POST['test'] = $_GET['test'];
            if(empty($_POST['answerType'])) $_POST['answerType'] = 0;
            $mapper->fillInstance($_POST, $question);

            if(!empty($_POST['questionText'])&&!empty($_POST['answerType'])){
                $db->insertDataToDB(
                    "questions",
                    " questionText, answerType, test",
                    "'{$question->getQuestionText()}', '{$question->getAnswerType()}', '{$question->getTest()}'"
                );

                $this->redirect('Questions&test='.$_GET['test']);
            }
            else{
                $this->view('questions/QuestionsCreate',$question, $questionValidator->getQuestionInputError($_POST));
            }

        }

        public function delete(): void{
            $db = new DatabaseConnection();
            $db->deleteDataFromDB("questions","id={$_GET['question']}");
            $db->deleteDataFromDB("answers","question={$_GET['question']}");

            $this->redirect('Questions&test='.$_GET['test']);
        }

    }