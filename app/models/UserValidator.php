<?php
    namespace app\models;
    use app\models\User;

    class UserValidator{

        public function getUserInputError(array $data): string{
            $error = "Nebylo zadáno: ";
            if(empty($data['username'])) $error .= "Uživatelské jméno,";
            if(empty($data['name'])) $error .= "Jméno,";
            if(empty($data['surname'])) $error .= "Příjmení,";
            if(empty($data['age'])) $error .= "Věk,";
            if(empty($data['sex'])) $error .= "Pohlaví,";
            return $error;
        }
    }