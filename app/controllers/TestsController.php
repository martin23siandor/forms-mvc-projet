<?php
    namespace app\controllers;

    use app\controllers\mappers\Mapper;
    use app\models\Answer;
    use app\models\DatabaseConnection;
    use app\models\Exam;
    use app\models\ExamAnswer;
    use app\models\Question;
    use app\models\Test;
    use app\models\User;

    class TestsController extends BaseController {

        public function index(): void{
            $db = new DatabaseConnection();
            $data = $db->getDataFromDB("tests", "*","")->fetch_all(MYSQLI_ASSOC);

            $mapper = new Mapper();
            $test = new Test();
            $error = "";
            $tests = [];

            !empty($data) ? $mapper->fillMultipleInstances($data, $test, $tests) : $error .= "Nejsou dostupná žádná data.";

            $this->view("tests/Test", $tests, $error);
        }

        public function add(): void{
            $data = "";
            $this->view("tests/TestCreate", $data);
        }

        public function create(): void{
            $db = new DatabaseConnection();

            $test = new Test();
            $mapper = new Mapper();
            $error = "";

            if(empty($_POST['id'])) $_POST['id'] = 0;
            if(empty($_POST['minSuccess'])) $_POST['minSuccess'] = 0;

            $mapper->fillInstance($_POST, $test);

            if(!empty($_POST['name'] && $_POST['minSuccess'])){
                $db->insertDataToDB(
                    " tests",
                    " name, minSuccess",
                    "'{$test->getName()}', {$test->getMinSuccess()}"
                );

                $this->redirect('Tests');
            }
            else{
                if(empty($_POST['name'])) $error .= " název testu nebyl zadán!";
                if(empty($_POST['minSuccess'])) $error .= " nebyla zadána minimální úspěšnost!";
                $error .= "😠";
                $this->view('tests/TestCreate',$test, $error);
            }
        }

        public function delete(): void{
            $db = new DatabaseConnection();
            $questionData = $db->getDataFromDB("questions", "*", "where test={$_GET['test']}")->fetch_all(MYSQLI_ASSOC);

            $questionInstance = new Question();
            $questions = [];

            $mapper = new Mapper();
            $mapper->fillMultipleInstances($questionData, $questionInstance, $questions);

            $db->deleteDataFromDB("tests","id={$_GET['test']}");
            $db->deleteDataFromDB("questions", "test={$_GET['test']}");

            foreach($questions as $question) $db->deleteDataFromDB("answers", "question={$question->getId()}");

            $this->redirect('Tests');
        }

        public function fill(): void{
            $db = new DatabaseConnection();

            $userData = $db->getDataFromDB("users", "*", "")->fetch_all(MYSQLI_ASSOC);
            $questionData = $db->getDataFromDB("questions", "*", "where test={$_GET['test']}")->fetch_all(MYSQLI_ASSOC);
            $answerData = $db->getDataFromDB("answers", "*", "")->fetch_all(MYSQLI_ASSOC);

            $userInstance = new User();
            $questionInstance = new Question();
            $answerInstance = new Answer();


            $users = [];
            $questions = [];
            $answers = [];

            $mapper = new Mapper();

            $mapper->fillMultipleInstances($userData, $userInstance, $users);
            $mapper->fillMultipleInstances($questionData, $questionInstance, $questions);
            $mapper->fillMultipleInstances($answerData, $answerInstance, $answers);

            $allData = ["users" => $users, "questions" => $questions, "answers" => $answers];

            $this->view('tests/TestFill', $allData);
        }

        public function saveExam(): void{
            $db = new DatabaseConnection();

            $questionData = $db->getDataFromDB("questions", "*", "where test={$_GET['test']}")->fetch_all(MYSQLI_ASSOC);
            $answerData = $db->getDataFromDB("answers", "*", "")->fetch_all(MYSQLI_ASSOC);
            $user = $db->getDataFromDB("users", "*", "where username = '{$_POST['username']}'")->fetch_assoc();

            $mapper = new Mapper();
            $examAnswer = new ExamAnswer();
            $exam = new Exam();
            $userInstance = new User();
            $questionInstance = new Question();
            $answerInstance = new Answer();

            $allData = [];
            $users = [];
            $questions = [];
            $answers = [];


            $mapper->fillInstance($user,$userInstance);
            $users[] = $userInstance;

            $mapper->fillMultipleInstances($questionData, $questionInstance, $questions);
            $mapper->fillMultipleInstances($answerData, $answerInstance, $answers);

            $data = ["users" => $users, "questions" => $questions, "answers" => $answers, "userAnswers" => $_POST];

            $examData = ["id" => 0, "result" => null, "user" => $userInstance->getId(), "test" => $_GET['test'], "createdOn" => date("Y-m-d H:i:s",time())];

            $mapper->fillInstance($examData, $exam);

            $keys = array_keys($_POST);
            $error = " ";
            $i = 0;
            $index = 0;

            if(!empty($_POST['submit'])){
                foreach(array_slice($keys,1) as $key){
                    $index++;
                    if(empty($_POST[$key])){
                        $i++;
                        if(str_starts_with($key, "abcAnswer")){
                            $error .= "nevybral jste žádnou možnost v {$index}. otázce, ";
                        }
                        else if(str_starts_with($key, "textAnswer")){
                            $error .= "nic jste nenapsal v {$index}. otázce, ";
                        }
                        else{
                            $error .= "nastala nějaká chyba";
                        }
                        $error .= "😠";
                    }
                }
            }

            if($i === 0){

                $db->insertDataToDB(
                    "exam",
                    " user, test, createdOn",
                    " '{$exam->getUser()}', '{$exam->getTest()}','{$exam->getCreatedOn()}'"
                );

                $lastExamId = $db->getLastInsertedId();

                foreach($keys as $item){
                    if(str_starts_with($item,"text")){
                        $questionId = str_replace("textAnswer", "", $item);
                        $db->insertDataToDB(
                            "examanswers",
                            " textAnswer, question, exam",
                            " '{$_POST[$item]}', '{$questionId}', '{$lastExamId}'"
                        );
                    }
                    else if(str_starts_with($item,"abc")){
                        $questionId = str_replace("abcAnswer", "", $item);
                        $db->insertDataToDB(
                            "examanswers",
                            " abcAnswer, question, exam",
                            " '{$_POST[$item]}', '{$questionId}', '{$lastExamId}'"
                        );
                    }
                }

                $this->redirect('Tests');

            }

            else{
                $this->view('tests/TestFill', $data, $error);
            }

        }
    }