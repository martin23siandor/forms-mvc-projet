<?php
    namespace app\controllers;
    use app\controllers\mappers\Mapper;
    use app\models\Answer;
    use app\models\DatabaseConnection;
    use app\models\Exam;
    use app\models\ExamAnswer;
    use app\models\Question;
    use app\models\Test;
    use app\models\User;

    class ExamController extends BaseController {
        public function index(): void{
             $db = new DatabaseConnection();
             $examInstance = new Exam();
             $userInstance = new User();
             $testInstance = new Test();
             $mapper = new Mapper();

             $testData = $db->getDataFromDB("tests", "*", " where id = ".$_GET['test'])->fetch_assoc();
             $mapper->fillInstance($testData, $testInstance);

             $examData = $db->getDataFromDB("exam", "*", " where test =".$testInstance->getId())->fetch_all(MYSQLI_ASSOC);
             $userData = $db->getDataFromDB("users", "*", "")->fetch_all(MYSQLI_ASSOC);

             $exams = [];
             $users = [];

            $mapper->fillMultipleInstances($examData, $examInstance, $exams);
            $mapper->fillMultipleInstances($userData, $userInstance, $users);


            $data = ["exams" => $exams, "users" => $users, "test" => $testInstance];

            $this->view("tests/exams/Exam", $data);
        }

        public function correct(): void{
            $db = new DatabaseConnection();
            $examData = $db->getDataFromDB("exam", "*", "where id=".$_GET['exam'])->fetch_assoc();

            $examInstance = new Exam();
            $answerInstance = new Answer();
            $questionInstance = new Question();
            $examAnswersInstance = new ExamAnswer();
            $mapper = new Mapper();
            $answers = [];
            $questions = [];
            $examAnswers = [];

            $mapper->fillInstance($examData, $examInstance);

            $answerData = $db->getDataFromDB("answers", "*", " where correctness=1")->fetch_all(MYSQLI_ASSOC);
            $questionData = $db->getDataFromDB("questions", "*", " where test={$examInstance->getTest()}")->fetch_all(MYSQLI_ASSOC);
            $examAnswersData = $db->getDataFromDB("examanswers", "*", " where exam={$examInstance->getId()}")->fetch_all(MYSQLI_ASSOC);

            $mapper->fillMultipleInstances($answerData, $answerInstance, $answers);
            $mapper->fillMultipleInstances($questionData, $questionInstance, $questions);
            $mapper->fillMultipleInstances($examAnswersData, $examAnswersInstance, $examAnswers);

            $data = ["userAnswers" => $examAnswers, "correctAnswers" => $answers, "questions" => $questions, "exam" => $examInstance];


            $this->view("tests/exams/ExamResults", $data);
        }

        public function showResult(): void{
            $db = new DatabaseConnection();

            $examData = $db->getDataFromDB("exam", "*", "where id=".$_GET['exam'])->fetch_assoc();
            $examInstance = new Exam();



            $testInstance = new Test();

            $answerInstance = new Answer();
            $questionInstance = new Question();
            $examAnswersInstance = new ExamAnswer();
            $mapper = new Mapper();
            $mapper->fillInstance($examData, $examInstance);

            $testData = $db->getDataFromDB("tests", "*", " where id=".$_GET['test'])->fetch_assoc();
            $answerData = $db->getDataFromDB("answers", "*", " where correctness=1")->fetch_all(MYSQLI_ASSOC);
            $questionData = $db->getDataFromDB("questions", "*", " where test=".$_GET['test'])->fetch_all(MYSQLI_ASSOC);
            $examAnswersData = $db->getDataFromDB("examanswers", "*", " where exam={$examInstance->getId()}")->fetch_all(MYSQLI_ASSOC);

            $mapper->fillInstance($testData, $testInstance);
            $answers = [];
            $questions = [];
            $examAnswers = [];

            $mapper->fillMultipleInstances($answerData, $answerInstance, $answers);
            $mapper->fillMultipleInstances($questionData, $questionInstance, $questions);
            $mapper->fillMultipleInstances($examAnswersData, $examAnswersInstance, $examAnswers);

            $data = ["userAnswers" => $examAnswers, "correctAnswers" => $answers, "questions" => $questions, "exam" => $examInstance, ];

            if(!empty($_POST['submit'])){

                if(!empty($_POST['result'])){
                    $correctAnswers = $_POST['result']['correctAbcAnswers'];
                    $numOfQuestions = $_POST['result']['numOfQuestions'];
                }

                $keys = array_keys($_POST);
                $i = 0;

                foreach($keys as $key){
                    if(empty($_POST[$key])){
                        if(str_starts_with($key, "check")){
                            $i++;
                        }
                    }
                    else{
                        if($_POST[$key] == "ano" || $_POST[$key] == "ne"){
                            if(str_starts_with($key, "check")){
                                if($_POST[$key] == "ano"){
                                    $correctAnswers++;
                                }
                            }
                        }
                    }
                }
                if($i ===0){
                    $result = ($correctAnswers/$numOfQuestions)*100;

                    $db->updateDataFromDB(
                        "exam",
                        "SET result='{$result}'",
                        "where id = ".$_GET['exam']
                    );

                    $this->redirect('Exam&test='.$_GET['test'].'&testName='.$testInstance->getName());
                }
                else{
                    $this->view("tests/exams/ExamResults", $data, "někde jste zapomněl vybrat možnost");
                }

            }

        }
    }