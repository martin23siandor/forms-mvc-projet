<?php
    if(empty($model['exams'])){
        $this->redirect('Tests');
    }
    else{
        echo "<h1>Vypracované testy {$_GET['testName']}</h1>";
        foreach($model['exams'] as $exam){
            $date = date("H:i:s d.m.Y", strtotime($exam->getCreatedOn()));
            echo "
                
                <div class='item'> 
                    <h2>Vyplněný test č. {$exam->getId()},</h2><h3> vytvořen {$date}</h3>
                    
                    Tento test vyplnil: 
            ";
            foreach($model['users'] as $user){
                if($user->getId() == $exam->getUser()){
                    echo $user->getUsername();
                }
            }

            if(is_null($exam->getResult())){
                echo "
                <h2 style='color: yellow;'>Tento test zatím nebyl vyhodnocen</h2>
                <div class='addButton'>
                    <a href='index.php?controller=Exam&action=correct&exam={$exam->getId()}&test={$_GET['test']}'>Opravit test</a>
                </div>";
            }
            else{
                $color = $exam->getResult() < 50 ? "#FF0000" : ( $exam->getResult() <= 75 ? "#FFDD00" : "#00FF00" );
                if($exam->getResult() < $model['test']->getMinSuccess()){
                    echo "<b><p style='color: {$color}'>NEUSPĚL! 😠</p></b>";
                }
                else{
                    echo "<b><p style='color: #00FF00'>USPĚL! 😊</p></b>";
                }
                echo "
            <h2 >Výsledek: <p style='color: {$color};'>{$exam->getResult()}%</p></h2>
        ";
            }
            echo "
            </div>
        ";
        }
    }

?>