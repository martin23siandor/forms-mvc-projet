<?php
    namespace app\models;

    class Answer{
        private int $id;
        private string $answerText;
        private ?int $question = null;
        private ?bool $correctness = null;

        /**
         * @return int
         */
        public function getId(): int
        {
            return $this->id;
        }

        /**
         * @param int $id
         */
        public function setId(int $id): void
        {
            $this->id = $id;
        }

        /**
         * @return string
         */
        public function getAnswerText(): string
        {
            return $this->answerText;
        }

        /**
         * @param string $answerText
         */
        public function setAnswerText(string $answerText): void
        {
            $this->answerText = $answerText;
        }

        /**
         * @return int|null
         */
        public function getQuestion(): ?int
        {
            return $this->question;
        }

        /**
         * @param int|null $question
         */
        public function setQuestion(?int $question): void
        {
            $this->question = $question;
        }

        /**
         * @return bool|null
         */
        public function isCorrectness(): ?bool
        {
            return $this->correctness;
        }

        /**
         * @param bool|null $correctness
         */
        public function setCorrectness(?bool $correctness): void
        {
            $this->correctness = $correctness;
        }



    }