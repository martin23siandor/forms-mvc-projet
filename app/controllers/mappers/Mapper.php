<?php
    namespace app\controllers\mappers;

    class Mapper{
        public function fillInstance(array $data, object &$object): void
        {
            $class = get_class($object);
            $methods = get_class_methods($class);
            $objInstance = new $class();

            foreach($methods as $setter){
                if(str_starts_with($setter,"set")){
                    $keyName = lcfirst(str_replace("set", "", $setter));
                    $objInstance->$setter($data[$keyName]);
                }
            }
            $object = $objInstance;
        }

        public function fillMultipleInstances(array $data, object $object, array &$arrayOfObjects): void{
            $class = get_class($object);
            $instance = new $class;

            foreach($data as $values){
                $this->fillInstance($values, $instance);
                $arrayOfObjects[] = $instance;
            }
        }
    }