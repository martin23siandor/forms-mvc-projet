<?php use app\models\Answer; ?>
<div class="button">
    <?php
        if(!empty($_GET['test'])){
            echo "<a href='index.php?controller=Questions&action=edit&question={$_GET['question']}&test={$_GET['test']}'>Zpět</a>";
        }
    ?>

</div>
<form class="addForm" method="POST" action=<?php echo "index.php?controller=Answers&action=create&question={$_GET['question']}";?>>
    <h2>Vytvořit novou odpověď:</h2>
    <input class="questionIdInput" name="question" hidden="hidden" type="number" readonly value=<?php echo $_GET['question']?>>
    <label>
        Text odpovědi:
        <input type="text" class="input" name="answerText">
    </label>
    <label>
        Je správná?
        <label>
            Ano
            <input type="radio" name="correctness" value=1>
        </label>
        <label>
            Ne
            <input type="radio" name="correctness" value=0 checked>
        </label>
    </label>
    <input type="submit" class="submitButton" value="Přidat odpověď">
</form>

<?php
    echo "<p class='error'>$message</p>";

    foreach($model as $answer){
        if($answer instanceof Answer){
            $checked = $answer->isCorrectness() ? "checked='checked'" : '';
            if($answer->getId() != 0 && !empty($answer->getId())){
                echo "
                    
                <form class='addForm' action='index.php?controller=Answers&action=update&answer={$answer->getId()}&question={$answer->getQuestion()}' method='POST'>
                    <label>
                    Odpověď č. {$answer->getId()}:
                    <input name='answerText' class='input' value='{$answer->getAnswerText()}'>
                    </label>
                    <label>
                        Správnost: 
                    <input class='radioInput' name='correctness' type='checkbox' value='{$answer->getId()}' {$checked}>
                    </label>  
                    <div class='deleteButton'>
                        <a href='index.php?controller=Answers&action=delete&answer={$answer->getId()}&question={$answer->getQuestion()}'>Odstranit odpověď</a>
                    </div>
                    <input type='submit' class='submitButton' value='Upravit odpověď'>
                </form>
                ";
            }
        }

    }

?>
