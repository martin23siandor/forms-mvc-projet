<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $getController = $_GET['controller'] ?? 'Index';

    spl_autoload_register(function ($name) {
        $path = '../'.str_replace('\\','/',$name).'.php';
        if(file_exists($path)) require $path;

    });

    $controllerName = 'app\\controllers\\'.$getController.'Controller';
?>
<html lang="cs">
    <head>
        <style>
            *{
                font-family: Calibri, serif;
                margin: 0;
                box-sizing: border-box;

            }
            body{
                background-color: whitesmoke;
            }
            .nav{
                background-color: steelblue;
                display: flex;
                flex-direction: row;
                height: 70px;
                align-items: center;
                justify-content: center;
            }
            .nav a{
                color: white;
                font-weight: bolder;
                text-decoration: none;
                font-size: 25px;
                margin: 0 30px;
            }
            .navItem{
                padding: 17px 50px;
                background-color: steelblue;
                transition-duration: 0.2s;
            }
            .navItem:hover{
                border-bottom: solid whitesmoke 5px;
                transition-duration: 0.3s;
            }
            .content{
                margin: 30px auto;
                min-height: 1000px;
            }
            .card{
                background-color: white;
                width: 70%;
                box-shadow: 2px 2px 5px lightslategrey;
                margin: auto;
                border-radius: 10px;
                padding: 50px;
            }
            .item{
                width: 90%;
                height: 250px;
                background-color: steelblue;
                border-radius: 10px;
                margin: 30px 50px;
                padding: 30px;
                box-shadow: 2px 2px 5px lightslategrey;
                color: white;
                transition-duration: 0.3s;
            }
            .item:hover{
                margin: 35px 55px;
                transition-duration: 0.3s;
            }
            .usersTable{
                width: 85%;
                margin: 30px auto;
                box-shadow: 2px 2px 5px lightslategrey;
                border-radius: 10px;
                background-color: #ced5dc;
                padding: 30px;
                color: steelblue;
            }
            .usersTable th{
                font-size: 30px;
                font-weight: bolder;
                color: #1f4464;

            }
            .usersTable tr{
                font-size: 20px;
            }
            .usersTable tr:hover{
                color: black;
                font-size: 22px;
                transition-duration: 0.2s;
            }
            .addButton{
                padding: 10px 20px;
                background-color: steelblue;
                color: white;
                border-radius: 10px;
                box-shadow: 2px 2px 5px lightslategrey;
                width: fit-content;
                margin-left: 80%;
            }
            .addButton:hover{
                background-color: #2f5675;
                padding: 12px 22px;
                transition-duration: 0.3s;
            }
            .addButton a{
                text-decoration: none;
                color: white;
            }
            .editUser a{
                text-decoration: none;
                color: black;
            }
            .editUser a:hover{
                color: lightslategrey;
                transition-duration: 0.2s;
            }
            .deleteButton{
                padding: 10px 20px;
                background-color: #cb3d3d;
                color: white;
                border-radius: 10px;
                box-shadow: 2px 2px 5px lightslategrey;
                width: fit-content;
                margin: 20px 10px;
            }
            .deleteButton a{
                color: white;
                text-decoration: none;
                padding: 10px;
            }
            .deleteButton a:hover{
                color: red;

            }
            .deleteButton:hover{
                background-color: whitesmoke;
                padding: 12px 22px;
                transition-duration: 0.3s;
            }
            .userId{
                height: 70px;
                background-color: steelblue;
                padding: 20px 20px;
                border-radius: 10px;
                font-size: 25px;
                color: white;
                margin: 30px auto;
                box-shadow: 2px 3px rgba(128, 121, 121, 0.116);
                width: 65%;
                display: flex;
                flex-direction: row;
                justify-content: space-around;
            }
            .userId input{
                border: none;
                height: 30px;
                background-color: rgba(0,0,0,0);
                width: 200px;
                font-size: 25px;
                color: #c3cfe2;
            }
            .userDetail{
                background-color: whitesmoke;
                width: 60%;
                padding: 30px 50px;
                margin: 30px auto;
                border-radius: 10px;
                box-shadow: 2px 3px rgba(128, 121, 121, 0.116);
            }
            .user{
                width: 80%;
                margin: 10px auto;
                font-size: 25px;
            }
            .userInput{
                border: none;
                height: 30px;
                font-size: 25px;
                background-color: rgba(0,0,0,0);
                width: 100px;
            }
            .submitButton{
                background-color: steelblue;
                color: white;
                font-weight: bold;
                font-size: 25px;
                padding: 20px 30px;
                border: 0;
                border-radius: 10px;
                margin: 30px auto;
                box-shadow: 2px 3px rgba(128, 121, 121, 0.116);
            }
            .submitButton:hover{
                background-color: #2f5675;
                transition-duration: 0.3s;
            }
            .addForm{
                display:flex;
                flex-direction: column;
                font-size: 25px;
                padding: 30px;
            }
            .addForm label{
                margin: 20px 50px;
            }
            .input{
                height: 50px;
                padding: 10px;
                width: 70%;
                border: 0;
                border-radius: 10px;
                background-color: whitesmoke;
                font-size: 25px;
                transition-duration: 0.2s;
            }
            .input:hover{
                background-color: #c3cfe2;
                transition-duration: 0.2s;
            }
            .input:focus{
                background-color: steelblue;
                color: white;
                transition-duration: 0.2s;
            }
            .error{
                font-size: 30px;
                color: red;
            }
            .item a{
                color: white;
                text-decoration: none;
            }
            .editP{
                font-size: 15px;
            }
            .questionP{
                font-size: 20px;
                margin: 30px;
                color: whitesmoke;
            }
            .footer{
            background-color: black;
            height: 50px;
            padding: 10px;
            }
            .footer a{
                text-decoration: none;
                color: white;
            }
            .itemA{
                padding: 10px;
                border-radius: 10px;
                background-color: #437298;
                width: fit-content;
                margin: 10px;
                box-shadow: 1px 1px 5px lightslategrey;
                transition-duration: 0.3s;
            }
            .itemA:hover{
                background-color: whitesmoke;
                transition-duration: 0.3s;
            }
            .itemA a{
                padding: 10px;
            }
            .itemA a:hover{
                transition-duration: 0.3s;
                color:steelblue;
            }
            .itemItems{
                display: flex;
            }
            .select{
                border:0 ;
                border-radius: 10px;
                background-color: whitesmoke;
                color: black;
                width: 30%;
                font-size: 25px;
                padding: 5px;
                transition-duration: 0.2s;
            }
            .select:hover{
                background-color: #ced5dc;
                transition-duration: 0.2s;
            }
            .select:focus {
                transition-duration: 0.2s;
                color: white;
                background-color: steelblue;
                padding: 7px;
            }
            .select:active {
                border: 0;
            }
            .examQuestionDiv{
                padding: 50px;
                border-radius: 10px;
                background-color: steelblue;
                box-shadow: 1px 1px 5px lightslategrey;
                margin: 30px;
                color: whitesmoke;
                transition-duration: 0.2s;
            }
            .examQuestionDiv:hover{
                background-color: #437298;
                transition-duration: 0.2s;
            }
            .examAnswerDiv{
                border-radius: 10px;
                display: flex;
                flex-direction: column;
                justify-content: space-around;
            }
            .correctMessage{
                color: #00ff00;
                font-size: 20px;
                font-weight: bolder;
            }
            .falseMessage{
                color: #ff0000;
                font-size: 20px;
                font-weight: bolder;
            }
            .button{
                background-color: white;
                color: steelblue;
                border-radius: 10px;
                border: solid #5795cb 2px;
                width: fit-content;
                padding: 10px;
                transition-duration: 0.3s;
                margin: 20px;
            }
            .button:hover{
                background-color: steelblue;
                color: white;
                border-radius: 10px;
                border: solid steelblue 2px;
                transition-duration: 0.3s;
            }
            .button a{
                text-decoration: none;
                color: steelblue;
                font-weight: bold;
                padding: 10px;
                transition-duration: 0.3s;
            }
            .button a:hover{
                color: white;
                transition-duration: 0.3s;
            }
            .homePageDiv{
                border-radius: 10px;
                color: black;
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: space-around;
            }
            .hpItem{
                background-color: white;
                border-radius: 20px;

                border: steelblue 3px solid;
                margin: 20px;
                transition-duration: 0.3s;
                padding: 50px 75px;
            }
            .hpItem:hover{
                background-color: steelblue;
                transition-duration: 0.3s;
            }
            .hpItem a{
                text-decoration: none;
                color: steelblue;
                padding: 50px 75px;
            }
            .hpItem a:hover{
                color:white;
            }


        </style>
        <title>Test</title>
    </head>
    <body>
        <div class="nav">
            <a href='index.php'>
                <div class="navItem">Domů</div>
            </a>
            <a href='index.php?controller=Users'>
                <div class="navItem">Uživatelé</div>
            </a>
            <a href='index.php?controller=Tests'>
                <div class="navItem">Testy</div>
            </a>
        </div>
        <div class="content">
            <div class="card">
                <?php
                if(class_exists($controllerName)){
                    $controllerInstance = new $controllerName();
                    $controllerMethodName = $_GET['action'] ?? 'index';
                    if(method_exists($controllerName,$controllerMethodName)){
                        $controllerInstance->$controllerMethodName();
                    }
                }
                else{
                    echo "Kontroler neexistuje";
                }
                ?>
            </div>
        </div>
        <div class="footer">
            <a href='index.php'>
                Domů
            </a>
        </div>
    </body>
</html>