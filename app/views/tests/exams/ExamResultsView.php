<form class="addForm" method="post" action="index.php?controller=Exam&action=showResult&exam=<?php echo $_GET['exam'];?>&test=<?php echo $_GET['test']?>">
    <div>
        <p class="error"><?php echo $message; ?></p>
        <h1>Vyplněný test č. <?php echo $model['exam']->getId();?> </h1>
        <h3><?php
                $date = date("H:i:s d.m.Y", strtotime($model['exam']->getCreatedOn()));
                echo $date;
            ?></h3>
    </div>
    <?php
    $numOfQuestions = count($model['questions']);
    $i = 0;
    foreach($model['questions'] as $question){
        echo "
                <div class='item'>
                    <h3>{$question->getQuestionText()}</h3>
                    Uživatel odpověděl:
            ";

        foreach($model['userAnswers'] as $examAnswer){
            if($examAnswer->getQuestion() == $question->getId()){

                if($question->getAnswerType() == 1){
                    echo $examAnswer->getAbcAnswer();
                    foreach($model['correctAnswers'] as $correctAnswer){
                        if($question->getId() == $correctAnswer->getQuestion()){
                            if($examAnswer->getAbcAnswer() == $correctAnswer->getAnswerText()){
                                echo "<p class='correctMessage'>Odpověď je správná.</p>";
                                $i++;
                            }
                            else{
                                echo "<p class='falseMessage'>Odpověď je špatně >:(</p>";
                            }
                        }
                    }
                }
                else{
                    echo $examAnswer->getTextAnswer();
                    echo "<p style='font-size: 20px; font-weight: bolder'>
                                Je tomu tak?
                                <input type='radio' hidden='hidden' checked value='0' name='check{$question->getId()}'>
                            <label class=''>
                                Ano
                                <input type='radio' value='ano' name='check{$question->getId()}'>
                            </label>
                            <label class=''>
                                Ne
                                <input type='radio' value='ne' name='check{$question->getId()}'>
                            </label>
                        </p>     
                    ";
                }
            }
        }
        echo "</div>";
    }
        $testResult = ($i/$numOfQuestions)*100;
        echo "
            <input type='hidden' value='{$i}' name='result[correctAbcAnswers]'>
            <input type='hidden' value='{$numOfQuestions}' name='result[numOfQuestions]'>
        ";
        echo "Správně je zatím {$i}  z {$numOfQuestions} odpovědí ({$testResult}%).";
    ?>
    <input type='submit' value='Odeslat' name='submit' class="submitButton">
</form>
