<div class="button">
    <a href=index.php?controller=Tests>Zpět na testy</a>
</div>

<?php echo !empty($_GET['testName']) ? "<h1>".$_GET['testName']. "</h1>" : ""; ?>


<div class="addButton">
    <a href=<?php echo empty($_GET['test'])?"index.php?controller=Questions&action=add":"index.php?controller=Questions&action=add&test={$_GET['test']}"?>>Přidat otázku</a>
</div>

<?php

use app\models\Question;

foreach($model as $question){
    if($question instanceof Question){
    ?>
    <div class="item">
        <h1>
            <a href="<?php echo "index.php?controller=Questions&action=edit&question={$question->getId()}&test={$question->getTest()}";?>">
                <div class="questionA">Otázka č. <?php echo $question->getId(); ?> <p class="editP">upravit</p></div>
            </a>
        </h1>
        <p class="questionP"> <?php echo $question->getQuestionText(); ?></p>
    </div>
    <?php
    }
}
?>
