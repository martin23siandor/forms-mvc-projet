<?php
    use app\models\User;
    $isInstance = $model instanceof User;

    if(!empty($model) && $isInstance){
    ?>
    <div class="deleteButton"><a href=<?php echo "index.php?controller=Users&action=delete&user=".$model->getId(); ?>>Vymazat</a></div>
    <?php
    }
    echo "<p class='error'>".$message."</p>";
?>

<form action=<?php echo "index.php?controller=Users&action=update"?> method="POST" >
    <div class="userId">
        <div>
            <label>Uživatelské jméno: </label>
            <input  class="userInput" name="username" type="text" value=<?php echo $isInstance ? $model->getUsername() : '' ?>>
        </div>
        <div>
            <label>ID:  </label>
            <input class="userInput" required readonly="readonly" name="id" type="text" value=<?php echo $isInstance ? $model->getId() : '' ?>>
        </div>
    </div>
    <div class="userDetail">
        <div class="user">
            <label>Jméno: </label>
            <input  class="userInput" name="name" type="text" value=<?php echo $isInstance ? $model->getName() : '' ?>>
            <input  class="userInput" name="surname" type="text" value=<?php echo $isInstance ? $model->getSurname() : ''?>>
        </div>
        <div class="user">
            <label>Věk: </label>
            <input  class="userInput" name="age" type="number" value=<?php echo $isInstance ? $model->getAge() : '' ?>>
        </div>
        <div class="user">
            <label>Pohlaví: </label>
            <input  class="userInput" maxlength="1" name="sex" type="text" value=<?php echo $isInstance ? $model->getSex() : '' ?>>
        </div>
    </div>
    <input type="submit" value="Odeslat" class="submitButton" style="margin-left: 43.5%;">
</form>
