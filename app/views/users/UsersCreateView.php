<?php
    use app\models\User;
    $isInstance = $model instanceof User;
    echo "<p class='error'>".$message."</p>";
?>
<form action="index.php?controller=Users&action=create" method="POST" class="addForm">
    <label>
        Uživatelské jméno:
        <input class="input" type="text" name="username" value=<?php echo $isInstance ? $model->getUsername() : ''?>>
    </label>
    <label>
        Jméno:
        <input class="input" type="text" name="name" value=<?php echo $isInstance ? $model->getName() : ''?>>
    </label>
    <label>
        Příjmení:
        <input class="input" type="text" name="surname" value=<?php echo $isInstance ? $model->getSurname() : ''?>>
    </label>
    <label>
        Věk:
        <input class="input" type="number" max="100" name="age" value=<?php echo $isInstance ? $model->getAge() : ''?> >
    </label>
    <label>
        Pohlaví:
        <input class="input" type="text" name="sex" value=<?php echo $isInstance ? $model->getSex() : ''?>>
    </label>
    <input type="submit" class="submitButton">
</form>