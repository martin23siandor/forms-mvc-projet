<?php
    namespace app\models;

    use mysqli;

    class DatabaseConnection{
        private string $servername = "localhost";
        private string $username = "root";
        private string $password = "";
        private string $db = "testdb";

        private int $lastId;

        public function connectToDB(){
            $conn = new mysqli($this->servername, $this->username, $this->password, $this->db);
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            return $conn;
        }

       public function getDataFromDB($table, $data, $where): mixed{
            $conn = $this->connectToDB();
            $sql = "SELECT {$data} from {$table} {$where}";

            $data = $conn->query($sql);

            $conn->close();

            return $data;
       }

        public function updateDataFromDB($table, $set, $where): mixed{
            $conn = $this->connectToDB();

            $sql = "UPDATE {$table} {$set} {$where}";

            $data = $conn->query($sql);

            $conn->close();

            return $data;
        }

        public function insertDataToDB($table,$columns,$values): void{
            $conn = $this->connectToDB();

            $sql = "INSERT into {$table} ({$columns}) values ({$values})";

            if (mysqli_query($conn, $sql)) {
                $this->setLastInsertedId($conn->insert_id);
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }

            $conn->close();

        }

        public function deleteDataFromDB($table, $condition): void{
            $conn = $this->connectToDB();

            $sql = "DELETE FROM {$table} where {$condition}";

            $data = $conn->query($sql);

            $conn->close();
        }

        public function setLastInsertedId($lastId): void{
            $this->lastId = $lastId;
        }

        public function getLastInsertedId(): int{
            return $this->lastId;
        }
    }
