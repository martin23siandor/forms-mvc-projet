<?php
    namespace app\models;

    class Test{
        private int $id;
        private string $name;
        private ?int $minSuccess = null;

        /**
         * @return int
         */
        public function getId(): int
        {
            return $this->id;
        }

        /**
         * @param int $id
         */
        public function setId(int $id): void
        {
            $this->id = $id;
        }

        /**
         * @return string
         */
        public function getName(): string
        {
            return $this->name;
        }

        /**
         * @param string $name
         */
        public function setName(string $name): void
        {
            $this->name = $name;
        }

        /**
         * @return int|null
         */
        public function getMinSuccess(): ?int
        {
            return $this->minSuccess;
        }

        /**
         * @param int|null $minSuccess
         */
        public function setMinSuccess(?int $minSuccess): void
        {
            $this->minSuccess = $minSuccess;
        }



    }