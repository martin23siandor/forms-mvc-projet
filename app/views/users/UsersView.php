<div class="addButton"><a href="index.php?controller=Users&action=add">Přidat uživatele</a></div>
<table class="usersTable">
    <tr>
        <th>ID</th>
        <th>Uživatelské jméno</th>
        <th>Jméno</th>
        <th>Příjmení</th>
        <th>Věk</th>
        <th>Pohlaví</th>
    </tr>
    <?php

    use app\models\User;

    foreach($model as $usr){
        ?>
        <tr>
            <?php
                if($usr instanceof User){
                    echo "
                       <td>{$usr->getId()}</td>
                       <td>{$usr->getUsername()}</td>
                       <td>{$usr->getName()}</td>
                       <td>{$usr->getSurname()}</td>
                       <td>{$usr->getAge()}</td>
                       <td>{$usr->getSex()}</td>
                    ";
                }
            ?>
            <td class="editUser">
                <a href=<?php echo "index.php?controller=Users&action=edit&user=".$usr->getId()?>>Upravit</a>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
